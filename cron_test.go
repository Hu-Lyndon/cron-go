package cron

import (
	"fmt"
	"testing"
	"time"
)

func TestNewCron(t *testing.T) {
	cron, err := NewCron("* * * * * *")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(cron.Match(time.Now()))
}
